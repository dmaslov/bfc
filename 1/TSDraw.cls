USING Progress.Lang.*.
CLASS TSDraw IMPLEMENTS ITableDraw:

{set-prop.i &aa="p" &cc="TTable2"}
{set-prop.i &aa="currLine"  &cc="INT64" &init="1"}

{set-prop.i &aa="lineCount" &cc="TAArray"}

{ttable.def}

CONSTRUCTOR TSDraw():

END CONSTRUCTOR.

METHOD PUBLIC VOID setParent(INPUT p AS TTable2):
	THIS-OBJECT:p = CAST(p,TTable2).
END METHOD.

METHOD PUBLIC VOID show():
  DEF VAR hQuery   AS HANDLE     NO-UNDO.
  DEF VAR hBuffer  AS HANDLE     NO-UNDO.
  DEF VAR prevRow  AS INT INIT 0 NO-UNDO.
  DEF VAR currRow  AS INT INIT 0 NO-UNDO.
  DEF VAR currCell AS INT INIT 0 NO-UNDO.
 
  hBuffer = p:getTable().

  CREATE QUERY hQuery.
  
  hQuery:SET-BUFFERS(hBuffer).
  hQuery:QUERY-PREPARE("FOR EACH tTable BY y-beg BY x-beg").
  hQuery:QUERY-OPEN().
  hQuery:GET-FIRST().

  currRow = hBuffer::y-beg.

  REPEAT WHILE NOT hQuery:QUERY-OFF-END:    

	 CREATE ttable.    
     BUFFER ttable:BUFFER-COPY(hBuffer).
     
     IF currRow <> hBuffer::y-beg THEN DO:
          onShowLine(currRow).
          currRow = hBuffer::y-beg.
          EMPTY TEMP-TABLE ttable.
     END.
     
     onShowCell(currCell,currRow,hBuffer).
     hQuery:GET-NEXT().
  END.  
   onShowLine(currRow).

  onAllComplete().
END METHOD.



METHOD PROTECTED VOID onShowLine(INPUT currLine AS INT64):
    DEF VAR width     AS INT        NO-UNDO.
    DEF VAR lineCount AS INT INIT 0 NO-UNDO.
    DEF VAR subStr    AS CHAR       NO-UNDO.
    DEF VAR i         AS INT        NO-UNDO. 
    /*DEF VAR oValue    AS TCharacter NO-UNDO.*/    

/** ����⠥� �� ᪮�쪮 ����� �㤥� ���� ��ப� **/
    FOR EACH ttable:
        width = p:getColWidth(ttable.x-beg).
        lineCount = MAXIMUM(lineCount,calcWordWrap(val,width)).      
    END.
   DISPLAY width.
   /** �뢮��� ����� �� ��࠭ **/    
   DO i = 1 TO lineCount:
     FOR EACH ttable:
        width = p:getColWidth(ttable.x-beg).
        subStr = getWordWrap(val,width,i).
        subStr = FSPACE(subStr,width,"right").
        PUT UNFORMATTED subStr "|".               
     END.
     PUT UNFORMATTED SKIP.
   END.
   PUT UNFORMATTED SKIP.     
END METHOD.

/**
 * @VAR CHAR cStr ��室��� ��ப�
 * @VAR INT width  �ॡ㥬�� �ਭ�
 * @VAR CHAR type ��� ��ࠢ������� {left,center,right}
 * @RETURN CHAR 
 * �㭪�� ��������
 * ��ப� �஡�����,
 * �� �������� �ਭ�.
 **/
METHOD PROTECTED CHAR FSPACE(INPUT cStr AS CHAR,INPUT width AS INT,INPUT type AS CHAR):
   DEF VAR diff AS INT NO-UNDO.
   diff = width - LENGTH(cStr).
   IF diff > 0 THEN cStr = FILL(" ",diff) + cStr.    
   RETURN cStr.
END METHOD.    

METHOD CHAR getWordWrap(INPUT cStr AS CHAR,INPUT width AS INT,INPUT iLine AS INT):
    
    DEF VAR currLine  AS INT INIT 1 NO-UNDO.
    DEF VAR strCount  AS INT INIT 0 NO-UNDO.
    DEF VAR i         AS INT        NO-UNDO.
    DEF VAR lastSpace AS INT        NO-UNDO.
    
    DEF VAR c        AS CHAR NO-UNDO.
    
DO WHILE cStr <> "" AND LENGTH(cStr) > width:      
  DO i = 1 TO LENGTH(cStr):
      c = SUBSTRING(cStr,i,1).
      IF c = " " THEN lastSpace = i.
      IF i = width THEN DO:

          IF c = " " OR lastSpace = 0 THEN DO:
              SUBSTRING(cStr,1,i) = "".
              strCount = strCount + 1.
              LEAVE.
          END. /* IF c = "" */ ELSE DO:
              SUBSTRING(cStr,1,lastSpace) = "".
              strCount = strCount + 1.
              LEAVE.
          END.
      END. 
  END.
  IF currLine = iLine THEN DO:
      RETURN cStr.
  END.
  currLine = currLine + 1.  
END. /* WHILE */   
END METHOD.    
METHOD INT calcWordWrap(INPUT cStr AS CHAR,INPUT width AS INT):
    DEF VAR currLine  AS INT INIT 1 NO-UNDO.
    DEF VAR strCount  AS INT INIT 0 NO-UNDO.
    DEF VAR i         AS INT        NO-UNDO.
    DEF VAR lastSpace AS INT        NO-UNDO.
    
    DEF VAR c        AS CHAR NO-UNDO.
    
DO WHILE cStr <> "" AND LENGTH(cStr) > width:      
  DO i = 1 TO LENGTH(cStr):
      c = SUBSTRING(cStr,i,1).
      IF c = " " THEN lastSpace = i.
      IF i = width THEN DO:

          IF c = " " OR lastSpace = 0 THEN DO:
              SUBSTRING(cStr,1,i) = "".
              strCount = strCount + 1.
              LEAVE.
          END. /* IF c = "" */ ELSE DO:
              SUBSTRING(cStr,1,lastSpace) = "".
              strCount = strCount + 1.
              LEAVE.
          END.
      END. 
  END.
  currLine = currLine + 1.  
END. /* WHILE */
RETURN strCount.
END METHOD.

METHOD PROTECTED VOID onShowCell(INPUT X AS INT,INPUT Y AS INT,INPUT Line AS HANDLE):

END METHOD.

METHOD PROTECTED VOID onAllComplete():

END METHOD.

END CLASS.