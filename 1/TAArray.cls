/**************************
 * ��������� ���
 ***************************/
CLASS TAArray IMPLEMENTS IIteration:

DEF PROTECTED TEMP-TABLE tbl_aarray NO-UNDO
						   FIELD id    AS INT64
						   FIELD code  AS  CHARACTER
						   FIELD val AS CHARACTER
						   INDEX idx_id   id
						   INDEX idx_code code   
						.


{set-prop.i &aa="length"  &bb="ilength" &cc="INT64" &init="0"       perms="PRIVATE"}
{set-prop.i &aa="curr"    &bb="iCurr"   &cc="INT64" &init="0"       perms="PRIVATE"}
{set-prop.i &aa="EOA"     &bb="lEOA"    &cc="LOGICAL" &init="TRUE"  perms="PRIVATE" perm="PROTECTED"}


CONSTRUCTOR TAArray():

END METHOD.

/*******

  ��⮤ �������� ���祭�� � ���

 *******/

METHOD VOID setH(INPUT cKey AS INT,INPUT cValue AS INT):
   setH(STRING(cKey),STRING(cValue)).
END METHOD.


METHOD VOID setH(INPUT cKey AS INT64,INPUT cValue AS INT64):
   setH(STRING(cKey),STRING(cValue)).
END METHOD.


METHOD VOID setH(INPUT cKey AS DATE,INPUT cValue AS DECIMAL):
   setH(STRING(cKey),STRING(cValue)).
END METHOD.

METHOD VOID setH(INPUT cKey AS CHARACTER,INPUT cValue AS DECIMAL):
   setH(cKey,STRING(cValue)).
END METHOD.

METHOD VOID setH(INPUT cKey AS CHARACTER,INPUT cValue AS INT64):
   setH(cKey,STRING(cValue)).
END METHOD.

METHOD VOID setH(INPUT cKey AS CHARACTER,INPUT cValue AS LOGICAL):
   setH(cKey,STRING(cValue)).
END METHOD.

METHOD VOID setH(INPUT cKey AS CHARACTER,INPUT cValue AS CHARACTER):

/***************************
 *			   *
 * ��⠭�������� 㪠��⥫� *
 * �� ����� ������.       *
 *			   *
 ****************************/ 
EOA = FALSE.
FIND FIRST tbl_aarray WHERE code = cKey EXCLUSIVE-LOCK NO-ERROR.

	IF AVAILABLE(tbl_aarray) THEN DO:
	   tbl_aarray.val = cValue.
	END.
	ELSE DO:
		length = length + 1.
		CREATE tbl_aarray.

		ASSIGN
			tbl_aarray.id   = length
			tbl_aarray.code = cKey
			tbl_aarray.val  = cValue
		.
	  END.
END METHOD.

/*****************************
 * ����砥� ���祭�� 
 *****************************/
METHOD CHARACTER get(INPUT cKey AS CHARACTER):

	FIND FIRST tbl_aarray WHERE tbl_aarray.code = cKey NO-LOCK NO-ERROR.
	
	IF AVAILABLE(tbl_aarray) THEN DO:
		RETURN tbl_aarray.val.
	END.

	RETURN ?.
END METHOD.

METHOD CHARACTER get(INPUT cKey AS DATE):
  RETURN get(STRING(cKey)).
END METHOD.

METHOD CHARACTER get(INPUT cKey AS INT):
  RETURN get(STRING(cKey)).
END METHOD.

METHOD CHARACTER get(INPUT cKey AS INT64):
  RETURN get(STRING(cKey)).
END METHOD.


METHOD VOID dump():
 FOR EACH tbl_aarray:
  DISPLAY tbl_aarray.
 END.
END.
/*****************
 * ����砥� ���祭�� � 㢥��稢��� ���稪
 *****************/
METHOD CHARACTER get_(OUTPUT cKey AS CHARACTER):

  /***********
   * �᫨ � ���ᨢ� ��祣� ���,
   * � �����頥� ���� � ����ਬ,
   * �� ��祣� ���.
   ***********/
  IF length <= 0 THEN DO:
       EOA = TRUE.
       RETURN ?.
  END. 
  ELSE DO:

    iCurr = iCurr + 1.

    FIND FIRST tbl_aarray WHERE tbl_aarray.id = iCurr NO-LOCK NO-ERROR.

	IF AVAILABLE(tbl_aarray) THEN DO:
	  cKey = tbl_aarray.code.
	END.
                                        
    IF iCurr = length THEN EOA = TRUE.

  RETURN tbl_aarray.val.
 END.
END METHOD.

/**
 * �����頥� ��ࢮ� ���祭��.
 **/
METHOD CHARACTER getFirst():

	FIND FIRST tbl_aarray NO-LOCK NO-ERROR.
	
	IF AVAILABLE(tbl_aarray) THEN DO:
		RETURN tbl_aarray.val.
	END.

	RETURN ?.

END METHOD.

/**
 * �����頥� ��᫥���� ���祭�� ᯨ᪠.
 **/
METHOD CHARACTER  getLast():
	FIND LAST tbl_aarray NO-LOCK NO-ERROR.
	
	IF AVAILABLE(tbl_aarray) THEN DO:
		RETURN tbl_aarray.val.
	END.

	RETURN ?.
END METHOD.

/******************
 * ����뢠�� ���稪
 *******************/
METHOD VOID reset_():
  iCurr = 0.
  IF length > 0 THEN  EOA = FALSE.
END METHOD.

METHOD PUBLIC LOGICAL EOI():
  RETURN EOA.
END METHOD.


METHOD PUBLIC CHARACTER save2str():
  DEF VAR cRes  AS CHARACTER INITIAL "" NO-UNDO.

  FOR EACH tbl_aarray NO-LOCK:
	cRes = cRes + tbl_aarray.val + ",".
  END.

  cRes = RIGHT-TRIM(cRes,",").
 RETURN cRes.
END METHOD.

METHOD PUBLIC TAArray ext(INPUT oa AS TAArray):
 DEF VAR key1 AS CHARACTER NO-UNDO.
 DEF VAR val1 AS CHARACTER NO-UNDO.
 RETURN THIS-OBJECT.
END METHOD.

END CLASS.
/***
 {foreach oar key value}
    {&out} key value.
  {endforeach oar}
*/