CLASS TBank INHERITS TBase:
	/*******************************************************
	 * ����� ��� �⮡ࠦ���� ���ଠ樨 �� ����� *
	 *******************************************************/

{set-prop.i &aa="bic" &bb="cbic" &cc="CHARACTER"}		 			/* ��� ����� */
{set-prop.i &aa="corr-acct" &bb="ccorr-acct" &cc="CHARACTER"}		        /* ������ ����� */
{set-prop.i &aa="bank-name" &bb="cbank-name" &cc="CHARACTER"}	        /*�������� ����� */
{set-prop.i &aa="short-name" &bb="cshort-name" &cc="CHARACTER"}		/*�������� ����� */
{set-prop.i &aa="law-address" &bb="claw-address" &cc="CHARACTER"}	/*�������� ����� */
{set-prop.i &aa="town" &bb="ctown" &cc="CHARACTER"}				/*��த ����� */
{set-prop.i &aa="town-type" &bb="ctown-type" &cc="CHARACTER"}		/*��த ����� */
{set-prop.i &aa="mail-index" &bb="cmail-index" &cc="CHARACTER"}		/* ������ ����� */

CONSTRUCTOR TBank():

END CONSTRUCTOR.

CONSTRUCTOR TBank(INPUT iBic AS CHARACTER):
		/************************************
		 * ���樠�����㥬 �� ����       *
		 ************************************/
FIND FIRST banks-code WHERE bank-code = iBic NO-LOCK NO-ERROR.
IF AVAILABLE(banks-code) THEN
	DO:
		/*******************************************
		 *								*
		 * ���� � ����� ����� �������.  *
		 * 								*
		 *******************************************/

		bic = banks-code.bank-code.

		FIND FIRST banks WHERE banks.bank-id = banks-code.bank-id NO-LOCK NO-ERROR.
		IF AVAILABLE(banks) THEN
			DO:
					/**********************************
					 * ������� ���ଠ�� �� �����       *
					 * ������塞 ᢮��⢠			    *
					 **********************************/
					FIND FIRST banks-corr WHERE banks-corr.bank-corr=banks.bank-id NO-LOCK NO-ERROR.

					IF AVAILABLE(banks-corr) THEN corr-acct = banks-corr.corr-acct.
											  ELSE corr-acct = "".

					initByBuffer(BUFFER banks:HANDLE).
					/********
					   ��।��塞 �������
					********/

			END.		

	END.
END CONSTRUCTOR.

METHOD PRIVATE VOID initByBuffer(INPUT bBank AS HANDLE):
					/************************************
					 * ���樠�����㥬 ᢮��⢠ ��ꥪ�    *
					 *************************************/

	ASSIGN
		bank-name = bBank::name
		short-name = bBank::short-name
		law-address = bBank::law-address
		town = bBank::town
		town-type = bBank::town-type
	.
		/* ��� �� �࠭�� ������ � �⤥�쭮� ����. ��室����� ������ ���⮢� ���� */
		mail-index = TRIM(ENTRY(1,bBank::mail-address,",")).
END METHOD.

METHOD PUBLIC LOGICAL isValidBIC(INPUT cBic AS INTEGER):
					/*********************************************
					* �஢���� ����⢠��� ����� � ⠪�� �����  *
					**********************************************/

END METHOD.
METHOD PUBLIC CHARACTER getNostroAcct():
					/****************************************
					 * 							   *
					 * ��⮤ �����頥� ��� Nostro.  *
					 * �᫨ ⠪�� � ��� �����.		   *					
					 *							   *
					 *****************************************/
		FIND FIRST c-nostro WHERE c-nostro.corr-acct = corr-acct NO-LOCK NO-ERROR.
		IF AVAILABLE(c-nostro) THEN RETURN c-nostro.acct. ELSE RETURN ?.
END METHOD.

METHOD PUBLIC CHARACTER getNostroAcct(INPUT cAcct AS CHARACTER):
					/****************************************
					 * 							   *
					 * ��⮤ �����頥� ��� Nostro.  *
					 * �᫨ ⠪�� � ��� �����.		   *					
					 *							   *
					 *****************************************/
		FIND FIRST c-nostro WHERE c-nostro.corr-acct = cAcct NO-LOCK NO-ERROR.
		IF AVAILABLE(c-nostro) THEN RETURN c-nostro.acct. ELSE RETURN ?.
END METHOD.


END CLASS.