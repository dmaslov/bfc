USING Progress.Lang.*.

CLASS TTable2 IMPLEMENTS ITplElement,ITable:

/*
{set-prop.i &aa="currRow"    &cc="INT64" &init="0"}
{set-prop.i &aa="currCell"   &cc="INT64" &init="0"}
{set-prop.i &aa="drawObject" &cc="CLASS OBJECT"}

{set-prop.i &aa="colCount"      &cc="INT64" &init="0"}
{set-prop.i &aa="rowCount"      &cc="INT64" &init="0"}
{set-prop.i &aa="colsWidth"     &cc="TAArray"}
{set-prop.i &aa="colsWidthAuto" &cc="TAArray"}
*/

{ttable.def}

CONSTRUCTOR TTable2():
  setDraw(NEW TSDraw()).
  colsWidth     = NEW TAArray().
  colsWidthAuto = NEW TAArray().  
END CONSTRUCTOR.

CONSTRUCTOR TTable2(INPUT i AS INT):
  setDraw(NEW TSDraw()).
  colsWidth = NEW TAArray().
  colsWidthAuto = NEW TAArray().
END CONSTRUCTOR.    

METHOD PUBLIC HANDLE getTable():
  RETURN BUFFER tTable:HANDLE.
END METHOD.

METHOD PUBLIC TTable2 addRow():

ASSIGN
currRow  = currRow  + 1
currCell = 0
rowCount = rowCount + 1
.
RETURN THIS-OBJECT.
END METHOD.

METHOD PROTECTED TTable2 addCell(INPUT cValue AS CHARACTER,cType AS CHARACTER):

DEF VAR av AS INT64 NO-UNDO.

currCell = currCell + 1.

CREATE tTable.
ASSIGN
   x-beg  = currCell
   y-beg  = currRow  
   type   = cType
   val    = cValue
 .

   colCount = MAXIMUM(colCount,currCell).   

   av = INT64(colsWidthAuto:get(currCell)).
   av = IF av = ? THEN 0 ELSE av.

   colsWidthAuto:setH(currCell,MAXIMUM(av,LENGTH(val))).
  
RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 addCell(INPUT cValue AS CHARACTER):
 addCell(STRING(cValue),"CHARACTER").
RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 addCell(INPUT cValue AS DECIMAL):
 addCell(STRING(cValue),"DECIMAL").
RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 addCell(INPUT cValue AS DATE):
 addCell(STRING(cValue),"DATE").
RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 addCell(INPUT cValue AS INT64):
 addCell(STRING(cValue),"INT64").
RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 addCell(INPUT cValue AS INT):
 addCell(STRING(cValue),"INT").
RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 setDraw(INPUT cObject AS CLASS OBJECT):
 drawObject = cObject. 
 RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 setColWidth(INPUT iCol AS INT64,INPUT width AS INT64):
 colsWidth:setH(iCol,width).
 RETURN THIS-OBJECT.
END METHOD.

METHOD PUBLIC TTable2 setColWidth(INPUT width AS INT64): 
 RETURN setColWidth(currCell,width).
END METHOD.

METHOD PUBLIC TAArray getColsWidth():
 RETURN colsWidth.
END METHOD.

METHOD PUBLIC TAArray getColsWidthAuto():
 RETURN colsWidthAuto.
END METHOD.

METHOD PUBLIC INT64 getColWidth(INPUT iCol AS INT):
 /*RETURN INT64(ENTRY(iCol,cashColWidth)).*/
END METHOD.


METHOD PUBLIC VOID show():
    DEF VAR key1   AS CHAR NO-UNDO.
    DEF VAR value1 AS CHAR NO-UNDO.
  /*{foreach colsWidthAuto key1 value1}
    cashColWidth = cashColWidth + (IF colsWidth:get(iCol) <> ? THEN colsWidth:get(iCol) ELSE colsWidthAuto:get(iCol)) + ",".
  {endforeach colsWidthAuto}*/   
  CAST(drawObject,ITableDraw):setParent(THIS-OBJECT).
  CAST(drawObject,ITableDraw):show().
END METHOD.


METHOD PUBLIC VOID setPropertyByStr(INPUT cPropertyName AS CHARACTER,INPUT cPropertyValue AS CHARACTER):
END METHOD.

METHOD PUBLIC VOID setIsInTpl(INPUT cProp AS LOGICAL):

END METHOD.

/**
 * @var cFileName ���� � XML 䠩�� � XML ����묨
 * ���㦠�� ����� � XML 䠩�.
 **/
METHOD PUBLIC VOID dump(INPUT cFileName AS CHAR):
 TEMP-TABLE ttable:WRITE-XML("file",cFileName,TRUE,?,?,NO,NO).
END METHOD.

/**
 * @var cFileName ���� � XML 䠩�� � ����묨
 * @return LOG
 * ����㦠�� ����� �� XML 䠩��.
 **/
METHOD PUBLIC LOG load(INPUT cFileName AS CHAR):
    TEMP-TABLE ttable:READ-XML("file",cFileName,"EMPTY",?,?,?,?).
    RETURN TRUE.
END METHOD.         

DESTRUCTOR TTable2():
 DELETE OBJECT colsWidthAuto.
 DELETE OBJECT colsWidth.
END DESTRUCTOR.

END CLASS.