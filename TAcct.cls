USING Progress.Lang.*.
CLASS TAcct INHERITS TBase IMPLEMENTS IFinObj:
/***********************************************************
 * ����� ���� �।�⠢������ ��饭��� ��壠���᪨�   *
 * ���.                                                   *
 * ����: ��᫮� �. �.                                     *
 * ��� ᮧ�����: 12.05.2009                               *
 ************************************************************/

 /*** PROTECTED ***/





  DEF PRIVATE BUFFER bAcct FOR acct.             /* ������ � ��⠬� */
  DEF PRIVATE BUFFER bAcct-pos FOR acct-pos.     /* ������ ���⪮� �� ��⠬ */
  DEF PRIVATE BUFFER bAcct-Cur FOR acct-cur.
  DEF VAR lRAISE_ERROR AS LOGICAL INITIAL false NO-UNDO.

  DEF PRIVATE VAR dbMove AS DECIMAL INITIAL 0 NO-UNDO.                        /* �������� �� ������ */
  DEF PRIVATE VAR crMove AS DECIMAL INITIAL 0 NO-UNDO.                        /* �������� �� �।��� */

  DEF PRIVATE VAR lisRSPMD AS LOGICAL INITIAL FALSE.			 /* ����襭� �� ��᭮� ᠫ줮 �� ���� */

 /*********************************** 
  * ��।��塞 �६���� ⠡����   *
  * ��� ����� ���⪠ � �訡��� *
  ***********************************/

  {tacct.def}							     


  
                  
 /*** PUBLIC ***/ 
{set-prop.i  &aa="rubPos" &bb="dRubPos" &cc="DECIMAL"}                              /* ���⮪ �� ���� � �㡫��   */
{set-prop.i  &aa="valPos" &bb="dValPos" &cc="DECIMAL"}                                /* ���⮪ �� ���� � �����  */
{set-prop.i  &aa="user-id" &bb="cuser-id" &cc="CHARACTER"}                          /* �⢥��⢥��� �� ���                */
{set-prop.i  &aa="val" &bb="cval" &cc="INTEGER"}                                            /* ����� ���                                */
{set-prop.i  &aa="acct" &bb="acct-id" &cc="CHARACTER"}                                  /* ����� ���                                */
{set-prop.i  &aa="activity" &bb="cActivity" &cc="CHARACTER"}                         /* ��� ��⨢��/���ᨢ�� */
{set-prop.i  &aa="name-short" &bb="cname-short" &cc="CHARACTER"}             /* �������� ��� */

{set-prop.i  &aa="clSurrogate" &bb="cClSurrogate" &cc="CHARACTER"}            /* ��� �������� ��� */
{set-prop.i  &aa="open-date" &bb="dOpenDate" &cc="DATE"}                                /* ��� ������ ��� */
{set-prop.i  &aa="details" &bb="cDetails" &cc="CHARACTER"}                             /* �������� ��� */
{set-prop.i  &aa="type" &bb="cType" &cc="CHARACTER"}                                    /* ��� �������� ��� */
{set-prop.i  &aa="id" &bb="iId" &cc="INTEGER"}                                                   /* ��� �������� ��� */

{set-prop.i  &aa="contr-acct" &bb="cContrAcct" &cc="CHARACTER"  &init="?"}  	/* ���� ��� ��� 㪠������� */
{set-prop.i  &aa="contract"   &bb="cContract"  &cc="CHARACTER"  &init="?"}  	/* �����祭�� ��� */
{set-prop.i  &aa="bal-acct"   &bb="iBal-acct"  &cc="INTEGER"    &init="?"}	/* �����ᮢ� ��� */
{set-prop.i  &aa="tKau-id"    &bb="ctKau-id"   &cc="CHARACTER"    &init="?"}	/* ��� ID �� ⠡���� */


 DEFINE PUBLIC PROPERTY RAISE_ERROR AS LOGICAL
   GET:
      RETURN lRAISE_ERROR.
   END GET.
                                             
   PROTECTED SET (INPUT cProp AS LOGICAL):
      lRAISE_ERROR=cProp.
   END SET.


DEF PUBLIC PROPERTY isRSPMD AS LOGICAL
   GET:
			/*********************************************************
			 *										     *
			 * isRSPMD - isRedSaldoPermitMiddleDay		     *
			 *										     *
			 *********************************************************
			 * ��⠥���, �� ��᭮� ᠫ줮 ����᪠����	     *
			 * � ᫥����� �����:					     *
			 *   1.  �� ��� � ��⠭������ ࠧ�襭��;	     *
			 *   2.  ��� ����;						     *
			 *   3. ��� ��� �ਧ���� �����.				     *
			 *										     *
			 *********************************************************/
		IF getXAttr("�������줮") = "�।�०�����" OR CAN-FIND(acct WHERE acct.acct=contr-acct) OR  activity = "��"  OR activity = "-" THEN RETURN TRUE.	
		RETURN lisRSPMD.
   END GET.
   PROTECTED SET(INPUT cProp AS LOGICAL):
		lisRSPMD = cProp.
   END SET.     


DEF PUBLIC PROPERTY kau-id AS CHARACTER
GET():
  /************************
   * �������� � 蠡����
   * ��� ����� �࠭����, 
   * 1. � ४����� kau-id �����ᮢ��� ���;
   * 2. � ४����� kau-id ��楢��� ���;
   * 3. � ⠡��� acct-kau-id.
   ************************/
   FIND FIRST acct-kau-id WHERE acct-kau-id.acct=acct NO-LOCK NO-ERROR.

      IF AVAILABLE(acct-kau-id) THEN DO:
	RETURN acct-kau-id.kau-id.
      END.
      ELSE DO:
	IF tKau-id NE ? THEN DO:
	  RETURN tKau-id.
	END.
	ELSE
	  DO:
	   FIND FIRST bal-acct WHERE bal-acct.bal-acct EQ bal-acct NO-LOCK NO-ERROR.
		   IF AVAILABLE(bal-acct) THEN DO:
		       RETURN bal-acct.kau-id.
		   END.   
	  END. /*tKau-id NE ? */
       END. /*available(acct-kau-id)*/
 RETURN ?.
END GET.  
PROTECTED SET(INPUT cProp AS CHARACTER):

END SET.  

 CONSTRUCTOR TAcct():

 END CONSTRUCTOR.

 CONSTRUCTOR TAcct(INPUT cAcct AS CHARACTER):
    /********************************
     *                              *
     * ��������� �� ������ ���  *
     *                              *
     ********************************/
	
     acct=REPLACE(cAcct,"-","").
     getValByAcct().
 END CONSTRUCTOR.

 /**********
  * @var CHARACTER cContract  ��� ������� {�।��,�����}
  * @var CHARACTER cCont-Code ����� �������
  * @var CHARACTER cRol       ���� ���
  * @var DATE      dDate      ��� �� ������ �ॡ���� ������� ���
  **********
  * ���樠������� ��� �� �������� � ஫�
  ***********/
 CONSTRUCTOR TAcct(INPUT cContract AS CHARACTER,INPUT cCont-Code AS CHARACTER,INPUT cRole AS CHARACTER,INPUT dDate AS DATE):

    /************
     * ������, �� ᪮� �����
     * ����������� �모�뢠��� �᪫�祭��.
     ************/
    FIND FIRST loan-acct WHERE     loan-acct.contract  = cContract
			       AND loan-acct.cont-code = cCont-Code
			       AND loan-acct.acct-type = cRole
			       AND loan-acct.since    <= dDate NO-LOCK NO-ERROR.
	IF AVAILABLE(loan-acct) THEN DO:
		getValByAcct(loan-acct.acct).	
        END.
        ELSE DO:

        END.
 END CONSTRUCTOR.



 METHOD PUBLIC LOGICAL add2DB():

    RETURN TRUE.
 END METHOD.
  
 METHOD PUBLIC LOGICAL update2DB():

 END METHOD.
 
 METHOD PRIVATE DECIMAL calcPos():
   /*************************************
    *                                   *
    * ��⠥� ���⮪ �� ᥣ����譨� ����. *
    *************************************/
   calcPos(TODAY). 
 END METHOD.

/**********
 * �� ���� ��� ࠧ���������
 * ᢮��⢮ ����� � 
 * ���祭�� �����.
 * ��室���� ������ ��� ⠪��
 * ������.
 * :-(
 **********/
METHOD PRIVATE VOID getValByAcct(INPUT cAcct AS CHARACTER):
  acct = cAcct.
  getValByAcct().
END METHOD.
METHOD PRIVATE VOID getValByAcct():
  /********************************************************
    * ���樠�����㥬 ᢮��⢠ ���                       *
    *******************************************************/
   FIND FIRST bAcct WHERE bAcct.acct=acct NO-LOCK NO-ERROR.
   IF AVAILABLE(bAcct) THEN
     DO:
       cActivity=bAcct.side.
       val=INTEGER(SUBSTR(acct,6,3)) NO-ERROR.
       name-short = bAcct.details.
       contr-acct = bAcct.contr-acct.
       contract   = bAcct.contract.
       bal-acct   = bAcct.bal-acct.
       tKau-id	  = bAcct.kau-id.


       IF ERROR-STATUS:ERROR THEN
          DO: 
             /* 
               �������� ���
               祣� � ���� ������
               ���� �� ����.
             */
          END.
       dOpenDate = DATE(bAcct.open-date).
       cDetails = bAcct.details.
       cType = cust-cat.
       iId = cust-id.

       ClassType = "acct".
       ClassCode = bAcct.Class-Code.
       Surrogate = acct + ",".
       IF val <> 810 THEN Surrogate = acct + "," + STRING(val).

	/**************************
	 * �᫨ ��� ����७���,  *
	 * � cust-id = ?         *
	 * ? + ��ப� = ?         *
         **************************/
       clSurrogate = cust-cat + "," + (IF STRING(cust-id) EQ ? THEN "" ELSE STRING(cust-id)).

     END.
     ELSE
      DO:
       RAISE_ERROR=TRUE.
      END.
     
END METHOD.

METHOD PUBLIC DECIMAL getRurPos(INPUT cBegDate AS DATE,OUTPUT dLastPosDate AS DATE):
    /*****************************************
     *                                                                                     *
     * �����頥� �㡫��� ���⮪ ���           *
     *                                                                               *
     *****************************************/
    RETURN getBegPosInVal(810,cBegDate,dLastPosDate).
END METHOD.

METHOD PUBLIC DECIMAL getBegPos(INPUT cBegDate AS DATE,OUTPUT dLastPosDate AS DATE):
    /*****************************************
     *                                       *
     * �����頥� ���⮪ � ����� ��� ��� *
     * � ⮩ ����� � ���ன �� ����        *
     *                                       *
     *****************************************/   
   RETURN getBegPosInVal(val,cBegDate,dLastPosDate).
END METHOD.

METHOD PRIVATE DECIMAL getBegPosInVal(INPUT acctVal AS INTEGER,INPUT cBegDate AS DATE,OUTPUT dLastPosDate AS DATE):
   /***************************************************** *
    * �㭪�� �����頥� ��砫�� ���⮪                *
    * �������, �� ��㬥�� dLastPosDate �����            *
    * ��७��� � ᢮��⢠ �����, �� ���� �� �� 㢥७. *
    * �㭪�� ��室�� ��� ������ ⠪ � �㡫��� ���⮪.*
    *******************************************************/
       DEF VAR oSysClass      AS TSysClass NO-UNDO.                        /* ������砥� ������⥪� � �㦥��묨 �㭪�ﬨ */
       DEF VAR cQuery         AS CHARACTER NO-UNDO.
       DEF VAR dLastCloseDate AS DATE      NO-UNDO.
       DEF VAR hBuffer        AS HANDLE    NO-UNDO.
       DEF VAR dRes           AS DECIMAL   NO-UNDO.
       
      oSysClass = new TSysClass().

       dLastCloseDate = DATE(oSysClass:getLastCloseDate(cBegDate)).


              
       cQuery="WHERE <TableWithPos>.acct=" + QUOTER(acct-id) + " AND <TableWithPos>.since<=" + QUOTER(cBegDate).

       /* 
         ��ନ�㥬 ����� � ��⮬ ������
       */

       IF acctVal EQ ? OR acctVal = 810 THEN
         DO:
          hBuffer= BUFFER bAcct-pos:HANDLE.
          cQuery=REPLACE(cQuery,"<TableWithPos>","bAcct-Pos"). 
         END.
         ELSE
          DO:
           hBuffer=BUFFER bAcct-Cur:HANDLE.                     
           cQuery=REPLACE(cQuery,"<TableWithPos>","bAcct-Cur").      
          END.
         
          hBuffer:FIND-LAST(cQuery,NO-LOCK) NO-ERROR.
          
          IF hBuffer:AVAILABLE THEN
            DO:               
               /****************************************
		*                                      *
		* � ⠡��� acct-pos �࠭����          *
		* ���⮪ �� ���� � ���ன �뫮       *
		* ��������, ���⮬� �����頥�         *
		* ���ᨬ��쭮� �� ��᫥����� �����⮣� *
		* ��� ��� ���������� � ⠡���         *
		* acct-pos.			       *
	        ****************************************/
              dLastPosDate=MAXIMUM(dLastCloseDate,hBuffer:BUFFER-FIELD("since"):BUFFER-VALUE).
              dRes = ABS(DECIMAL(hBuffer:BUFFER-FIELD("balance"):BUFFER-VALUE)).
            END.
            ELSE
              DO:
                /* ���⮪ �� ��諨 */
		dLastPosDate = MINIMUM(dLastCloseDate,cBegDate - 1).
                dRes = 0.
              END.

DELETE OBJECT oSysClass.

RETURN dRes.
END METHOD.
 

METHOD PRIVATE VOID calcPos(INPUT dDate AS DATE):

   /***********************************
    *                                 *
    *  ��⠥� ���⮪ �� 㪠������   *
    * ����.                           *
    * ���뢠�� ��楯⮢���� ���-��. *
    *                                 *
    ***********************************/
    
   DEF VAR tmpPosSum AS DECIMAL INITIAL 0 NO-UNDO.
   
END METHOD.

METHOD PUBLIC DECIMAL getPos2DateVVK(INPUT dDate AS DATE,INPUT cStatus AS CHARACTER):
						/*************************************************************
						 * ��⮤ �����頥� �������� ������� �� ����!  *
						 * 1. �᫨ ��� �㡫���, � �㤥� �����饭 �����
						 * ���⮪ �� ����� ���.
						 * 2. �᫨ ��� ������:
						 *    2.1.  �㤥� ����祭 ������ ���⮪ �� ��砫� ���;
						 *    2.2. ������ ���⮪ 㬭���� �� ���� �� �� ���
						 *   ����; �᫨ ���� �� ��⠭�����, � � ����⢥ ���祭��
						 * �㤥� ��⠭������ 1.
						 *    2.3. ������ ���⮪ 㬭���� �� ���祭�� ����;
						 *    ����稬 �㡫��� ���������� � ��⮬ ��८業��
						 * (���� �᫨ ��८業�� �� �஢�������).
						 *   2.4 �� �㡫����� ���������� �㤥� ������
						 * �㬬� ������� ����権 �� ���� � �㡫��.
						 *
						 * ����������: �᫨ ��⮤ �㤥� �ਬ���� � �㡫�����
						 * ����, � ��⮤ ��᮫�⭮ �������祭 getPos2Date().
						 ***********************************************************/

DEF VAR oSysClass AS TSysClass NO-UNDO.

DEF VAR dCursCB AS DECIMAL NO-UNDO.

DEF VAR dSumRur AS DECIMAL NO-UNDO.
DEF VAR dSumVal AS DECIMAL NO-UNDO.
		
oSysClass = new TSysClass().

			dCursCB = oSysClass:getCBRKurs(val,dDate).								/* ����砥� ���� �� �� ���� */

			dSumVal = getLastPos2Date(dDate - 1,cStatus,val). 							 /* ���⮪ �� ��� � ����� */


			IF val <> 810 THEN
				DO:
					dSumRur = ROUND(dSumVal * (IF dCursCB <> ? THEN dCursCB ELSE 1),2).		/* ���⮪ � �㡫�� �� ��� */
				END.
				ELSE
					DO:
						dSumRur = dSumVal.
					END.
	
			       dSumRur = dSumRur + calcOborot (dDate - 1,dDate,cStatus,810).				/* ����⠫� ����� �� �㡫� �� ⥪�騩 ���� */

DELETE OBJECT oSysClass.
RETURN dSumRur.
END METHOD.

METHOD PUBLIC DECIMAL getLastPos2Date(INPUT dDate AS DATE,INPUT cStatus AS CHARACTER,INPUT iVal AS INTEGER):
                    /*****************************************************
		     *							 *
                     * �㭪�� �����頥� ��������� ���⮪  		 *
                     * �� ���� dDate � ��⮬ ���㬥�⮢                *
                     * � ����� cStatus, � ����� iVal.                 *
		     *							 *
                     *****************************************************/

DEF VAR dLastPosDate AS DATE    NO-UNDO.
DEF VAR dSum 	     AS DECIMAL NO-UNDO.
DEF VAR oSysClass AS TSysClass  NO-UNDO.

    oSysClass = new TSysClass().    
     dSum = getBegPosInVal(iVal,dDate,dLastPosDate).                   /* ����稫� ��᫥���� ���� �� ������ ���� ��ॣ�஢���� ���⮪ */
     dSum = dSum + calcOborot(dLastPosDate,dDate,cStatus,iVal).
   DELETE OBJECT oSysClass.
RETURN dSum.
END METHOD.

METHOD PUBLIC DECIMAL getLastPos2Date(INPUT dDate AS DATE,INPUT cStatus AS CHARACTER):
		/**********************************************
		 *					      *
		 * ��⮤ �����頥� ���⮪ �� ����          *
		 * � ����� ���.			      *
		 *					      *
		 **********************************************/
	RETURN getLastPos2Date(dDate,cStatus,val).
END METHOD.

METHOD PUBLIC DECIMAL getLastPos2Date(INPUT dDate AS DATE):
        /***********************************
         * �㭪�� �����頥� ���⮪     *
         * � ����� ���, �� ���� dDate. *
         * ���뢠�� �� ���㬥���,       *
         * �஬� 㤠������                *
         **********************************/
        RETURN getLastPos2Date(dDate,"").
END METHOD.


/******************
  ��⮤ �����頥� 
  ���� ��室 
  �� ID �࠭���樨 (������ �࠭�����
  �� ����᪥ ����� 㭨����� �����)
  �� �⮬ ���뢠���� ��室, �����
  �� �������� � ����� �᭮����� �����.
*******************/

METHOD PUBLIC DECIMAL calcOborot(INPUT iOpTr AS INTEGER):

DEF VAR dRes AS DECIMAL INITIAL 0 NO-UNDO.
  
  IF activity = "�" THEN DO:
	  FOR EACH op WHERE op-transaction EQ iOpTr NO-LOCK,
	    EACH op-entry OF op WHERE op-entry.acct-cr EQ acct 
                                      AND op-entry.op-status>="�" AND op-entry.op-status < "�" NO-LOCK:
	dRes = dRes + op-entry.amt-rub.
   END.
  END.
  ELSE DO:
	  FOR EACH op WHERE op.op-transaction EQ iOpTr NO-LOCK,
	    EACH op-entry OF op WHERE op-entry.acct-db EQ acct 
	    AND op-entry.op-status>="�" AND op-entry.op-status < "�" NO-LOCK:

	    	dRes = dRes + op-entry.amt-rub.
   END.
  END.
 RETURN dRes.
END METHOD.

METHOD PUBLIC DECIMAL  calcOborot (INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE,INPUT cStatus AS CHARACTER,INPUT iVal AS INTEGER):
	/****************************************
	 * 			                *
	 *  ��⮤ �����頥� �����             *
	 * �� ���� �� 㪠����� ��ਮ�.        *
	 *                                      *
	 ****************************************/
        RETURN calcOborot(dDateBeg,dDateEnd,cStatus,iVal,"*").
END METHOD.



METHOD PUBLIC DECIMAL  calcOborot (INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE,INPUT cStatus AS CHARACTER,INPUT iVal AS INTEGER,INPUT cTransaction AS CHAR):

								/******************************************
								 * 				          *
								 *  ��⮤ �����頥� �����               *
								 * �� ���� �� 㪠����� ��ਮ�  .        *
								 * �� �᪫�祭��� �࠭���権 cTransaction *
								 *					  *
								 ******************************************/

DEF VAR dSum             AS DECIMAL INITIAL 0 NO-UNDO.
DEF VAR dSumOpEntryWSign AS DECIMAL           NO-UNDO.
DEF VAR oSysClass        AS TSysClass         NO-UNDO.

oSysClass = new TSysClass().

     FOR EACH op-entry WHERE op-entry.acct-db=acct  				
						   AND dDateBeg<op-entry.op-date AND op-entry.op-date<=dDateEnd
						   AND op-entry.op-date<>? 
						   AND op-entry.op-status >= cStatus NO-LOCK,
						   FIRST op OF op-entry WHERE CAN-DO(cTransaction,op.op-kind) NO-LOCK:

		        IF iVal=810 THEN
		                 dSumOpEntryWSign = oSysClass:op-entry-sign("��",activity) * op-entry.amt-rub.
		        ELSE
		                 dSumOpEntryWSign = oSysClass:op-entry-sign("��",activity) * op-entry.amt-cur.

			         dSum = dSum + dSumOpEntryWSign.
        END. /* �� �஢����� � ����� */


     FOR EACH op-entry WHERE op-entry.acct-cr=acct 
						   AND dDateBeg<op-entry.op-date AND op-entry.op-date<=dDateEnd
						   AND op-entry.op-date<>? 
						   AND op-entry.op-status >= cStatus NO-LOCK,
						   FIRST op OF op-entry WHERE CAN-DO(cTransaction,op.op-kind) NO-LOCK: 
        /* ������ ������������ ���� !!! ���� ��-����� �ਤ㬠�� ! */
        IF iVal=810 THEN
            dSumOpEntryWSign = oSysClass:op-entry-sign("��",activity) * op-entry.amt-rub.
        ELSE
            dSumOpEntryWSign=oSysClass:op-entry-sign("��",activity) * op-entry.amt-cur.

         dSum = dSum + dSumOpEntryWSign.
	END. /* �� �஢����� � ����� */


DELETE OBJECT oSysClass.	

RETURN dSum.
END METHOD.

METHOD PROTECTED DECIMAL  calcOborot (INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE):

                                                                /*************************************
                                                                 *                                                                          *
                                                                 * ���樠������� ��६����                 * 
                                                                 * � ����⮬.                                                 *
                                                                 * � ���쭥�襬 ����� ᤥ����                 *
                                                                 * �஬������� ⠡���� ���                  *
                                                                 * �᪮७�� ������.                                 *
                                                                 *                                                                          *
                                                                 *************************************/
        DEF VAR oSysClass   AS TSysClass   NO-UNDO.                /* ������砥� �㦥��� �㭪樨 */
        DEF VAR oDocCollect AS TDocCollect NO-UNDO.        
        DEF VAR dLastDateWAccum AS DATE LABEL "��᫥���� ��� � ��ॣ�஢���� ���⪮�" NO-UNDO.
        DEF VAR i AS INTEGER NO-UNDO.
        DEF VAR j AS INTEGER NO-UNDO.

        oSysClass = new TSysClass().
        dLastDateWAccum = dDateBeg.

/*  ����ਬ �������� �� ������� ��� */
   FOR EACH acct-pos WHERE acct-pos.acct=acct-id AND since>=dDateBeg AND since<=dDateEnd NO-LOCK BY since:
        /* ��� �㡫�� */
       ACCUMULATE debit (TOTAL).        
       ACCUMULATE credit (TOTAL).
        
        dLastDateWAccum = since + 1.
       /* ��� �������� ���� ��� ����室����� */
   END. 

    dbMove = (ACCUM TOTAL debit).
    crMove = (ACCUM TOTAL credit).

/*  ����ਬ �������� �� ������ ��� */

/* 
   �� ��᫥����� �����⮣� ���,
    ᬮ�ਬ ���㬥���.
   �㬬��㥬 ��� ���⠥� �� �㬬�
    � ����ᨬ��� �� ��������� � �஢����.
�� ᯮ�� ������� ���������, 祬 ���⮩ ����� � ��, �� ��� 
� ����� �易���� � ��⮬ ��� ����� �痢� � ⠡��栬� OP � OPENTRY
*/

oDocCollect = new TDocCollect().
oDocCollect:date-beg = dLastDateWAccum.
oDocCollect:date-end = dDateEnd.
oDocCollect:acct-db = acct.
oDocCollect:acct-cr = acct. 
oDocCollect:beg-status = CHR(251).
oDocCollect:isAcctAND=false.
oDocCollect:applyFilter().

DO i = 1 TO oDocCollect:docCount:
   /* �� �ᥬ ���㬥�⠬ */
   DO j = 1 TO oDocCollect:getDocument(i):OpEntryCount:
        /* ����� �� �஢����� � ���㬥�� */
        IF oDocCollect:getDocument(i):getOpEntry4Order(j):acct-db = acct THEN 
                                                                                                                         DO:
                                                                                                                           dbMove = dbMove + oDocCollect:getDocument(i):getOpEntry4Order(j):rusSum.
                                                                                                                         END.
                                                                                                                           ELSE 
                                                                                                                            DO:
                                                                                                                                 crMove = crMove + oDocCollect:getDocument(i):getOpEntry4Order(j):rusSum. 
                                                                                                                            END.
  END.
END.

DELETE OBJECT oDocCollect.        
DELETE OBJECT oSysClass.

END METHOD.

METHOD PUBLIC DECIMAL getCrMove (INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE):

                                                             /****************************************
                                                              *                                                                            *
                                                              * �����頥� ����� �� �।���                    *
                                                              *                                                                            *
                                                              ****************************************/
   calcOborot(dDateBeg,dDateEnd).
   RETURN crMove.

END METHOD.

/**
 *
 * @param DATE dDateBeg - ��� ��砫� ��।������ �����;
 * @param DATE dDateEnd - ��� ����砭�� ��।������ �����;
 * @param CHARACTER cStatus - ����� � ���஬ ᬮ����� �����;
 * @return TAArray - �����頥� ���ᨢ amt-rub � amt-cur � ����⮬.
 *
 *                  !!! �������� !!!
 *                ���� �� ᮡ�� १����! 
 *            ���� ������� ���� �����!
 **/

METHOD PUBLIC TAArray getCrMove (INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE,INPUT cStatus AS CHARACTER):

   DEF VAR oa AS TAArray NO-UNDO.

   FOR EACH op-entry WHERE op-entry.acct-cr = acct 
		       AND op-entry.op-date >= dDateBeg AND op-entry.op-date <= dDateEnd AND op-entry.op-date <> ? 
		       AND op-entry.op-status >= cStatus
		       NO-LOCK,
		FIRST op OF op-entry NO-LOCK:

		 ACCUMULATE op-entry.amt-rub (TOTAL).
		 ACCUMULATE op-entry.amt-cur (TOTAL).
   END.

   oa = new TAArray().
	   oa:setH("amt-rub",(ACCUM TOTAL op-entry.amt-rub)).
	   oa:setH("amt-cur",(ACCUM TOTAL op-entry.amt-cur)).
  RETURN oa.
END METHOD.


METHOD PUBLIC DECIMAL getDbMove(INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE):

                                                             /****************************************
                                                              *                                                                            *
                                                              * �����頥� ����� �� ������                            *
                                                              *                                                                            *
                                                              ****************************************/
   calcOborot(dDateBeg,dDateEnd).
   RETURN dbMove.

END METHOD.

METHOD PUBLIC DATE getFirstMoveByDate(INPUT dDateBeg AS DATE,INPUT dDateEnd AS DATE,INPUT cwithCorr AS CHARACTER):
                            /*******************************************
                             * ��⮤ �����頥� ���� ��ࢮ�� ��������    *
                             * ��  ����.                                                                *
                             *******************************************/
         
FIND FIRST op-entry WHERE op-entry.acct-db = acct
                                                      AND op-entry.acct-cr  MATCHES cwithCorr  
                                                      AND op-entry.op-date>=dDateBeg AND op-entry.op-date<=dDateEnd 
                                                      NO-LOCK NO-ERROR.

 IF NOT AVAILABLE(op-entry) THEN 
    DO:
         FIND FIRST op-entry WHERE op-entry.acct-cr = acct 
                                                               AND op-entry.acct-db MATCHES cwithCorr 
                                                               AND op-entry.op-date>=dDateBeg AND op-entry.op-date<=dDateEnd 
                                                    NO-LOCK NO-ERROR.

         IF AVAILABLE(op-entry) THEN RETURN op-entry.op-date.
      END.
      ELSE 
        RETURN op-entry.op-date.

   RETURN ?.      
END METHOD.

/*************************
 *
 * ����砥� ����� ��
 * ����.
 *
 *************************/
METHOD PUBLIC DECIMAL getPos2DateWError (INPUT dDate AS DATE,INPUT cTransaction AS CHARACTER):

  DEF VAR h       AS OBJECT  NO-UNDO.
  DEF VAR dBegPos AS DECIMAL NO-UNDO.
  DEF VAR dRes    AS DECIMAL NO-UNDO.

  dRes = getPos2DateWError(dDate,cTransaction,dBegPos,h).
  DELETE OBJECT h.

 RETURN dRes.
END METHOD.

/*********************************************
 *                                           *
 *           !!! �������� !!!		     *
 *                                           *
 *  ����� ������������� ����� ������ hCalc!  *

 ***********************************************
 * ���� ��⮤�: 1. ��।����� ���⮪ �� ���� *
 * ���뢠� ����७�� �࠭���樨.	       *
 * 2. �뤠�� ����=>���祭�� �����.	       *
 *                                             *
 ***********************************************/

METHOD PUBLIC DECIMAL getPos2DateWError (INPUT dDate AS DATE,INPUT cTransaction AS CHARACTER,OUTPUT dBegPos AS DECIMAL,OUTPUT hCalc AS CLASS OBJECT):

DEF VAR oSysClass AS TSysClass         NO-UNDO.
DEF VAR oDBObRub  AS TAArray           NO-UNDO.
DEF VAR oCRObRub  AS TAArray           NO-UNDO.
DEF VAR oItogRub  AS TAArray           NO-UNDO.

DEF VAR oDBObCur  AS TAArray           NO-UNDO.
DEF VAR oCRObCur  AS TAArray           NO-UNDO.
DEF VAR oItogCur  AS TAArray           NO-UNDO.


DEF VAR dItogRub AS DECIMAL INITIAL 0  NO-UNDO.
DEF VAR dItogCur AS DECIMAL INITIAL 0  NO-UNDO.
DEF VAR dSum     AS DECIMAL INITIAL 0  NO-UNDO.

DEF VAR dLastPosDate AS DATE           NO-UNDO.
DEF VAR iDate 	     AS DATE           NO-UNDO.





DEF VAR cStatus1 AS CHARACTER          NO-UNDO.
DEF VAR cStatus2 AS CHARACTER          NO-UNDO.
DEF VAR cPirRsv2TrDwnSt AS CHARACTER   NO-UNDO.

/*** ���⪨ �� �㡫� ***/
oDBObRub = new TAArray().
oCRObRub = new TAArray().
oItogRub = new TAArray().

/*** ���⪨ �� ����� ***/
oDBObCur = new TAArray().
oCRObCur = new TAArray().
oItogCur = new TAArray().


oSysClass = new TSysClass().

/***************
 * ����稫� ᯨ᮪ 
 * ����७��� �࠭���権.
 ***************/
cPirRsv2TrDwnSt = oSysClass:getSetting("PirChkOp","PirRSv2TrDwnSt","!*").

/***************
 * ����稫� ��᫥���� ���� 
 * �� ������ ���� ��ॣ�஢���� 
 * ���⮪.
 ***************/
dSum = getBegPosInVal(val,dDate,dLastPosDate).        

/********************************** ������� �� �� ***********************************/

/**************************************************
 *						  *
 * �� �� ���㬥�⠬ � ������� ��ਮ�            *
 * � ��砥, �᫨ ��� �⮨� �� ��.		  *
 *                                                *
 ***************************************************/

IF activity EQ "�" THEN DO:
			 cStatus1 = "�".
			 cStatus2 = "�".
			END.

IF activity EQ "�" THEN DO:
			 cStatus1 = "�".
			 cStatus2 = "�".
		        END.

FOR EACH op-entry WHERE op-entry.acct-db=acct  				
			AND dLastPosDate<op-entry.op-date AND op-entry.op-date<=dDate
			AND op-entry.op-date<>? AND op-entry.op-status <> "" NO-LOCK,
			FIRST op OF op-entry WHERE CAN-DO(cTransaction,op.op-kind)
						   AND (op.op-status >= cStatus1 OR (CAN-DO(cPirRsv2TrDwnSt,op.op-kind) AND op.op-status>=cStatus2))
						   AND op.op-date<>? NO-LOCK
                                                   BREAK BY op-entry.op-date:

   ACCUMULATE op-entry.amt-rub (SUB-TOTAL BY op-entry.op-date).
   ACCUMULATE op-entry.amt-cur (SUB-TOTAL BY op-entry.op-date).

   IF LAST-OF(op-entry.op-date) THEN
       DO:
		oDBObRub:setH(op-entry.op-date,(ACCUM SUB-TOTAL BY op-entry.op-date op-entry.amt-rub)).
		oDBObCur:setH(op-entry.op-date,(ACCUM SUB-TOTAL BY op-entry.op-date op-entry.amt-cur)).
      END.	/* ����� ������ ��᫥���� ������� ��㯯� */
END. /* ����� �� �஢����� */


/****************************************************
 *  �� �� ���㬥�⠬ � ������� ��ਮ�      *
 * � ��砥, �᫨ ��� �⮨� �� ��.		     *
 *                                                                           *
 ***************************************************/

FOR EACH op-entry WHERE op-entry.acct-cr=acct  			
			AND dLastPosDate<op-entry.op-date AND op-entry.op-date<=dDate
			AND op-entry.op-date<>? AND op-entry.op-status <> "" NO-LOCK,
			FIRST op OF op-entry WHERE CAN-DO(cTransaction,op.op-kind)
						   AND (op.op-status >= cStatus2 OR (CAN-DO(cPirRsv2TrDwnSt,op.op-kind) AND op.op-status>=cStatus1))
						   AND op.op-date<>? NO-LOCK
						   BREAK BY op-entry.op-date:

   ACCUMULATE op-entry.amt-rub (SUB-TOTAL BY op-entry.op-date).
   ACCUMULATE op-entry.amt-cur (SUB-TOTAL BY op-entry.op-date).

   IF LAST-OF(op-entry.op-date) THEN
       DO:
	oCRObRub:setH(op-entry.op-date,(ACCUM SUB-TOTAL BY op-entry.op-date op-entry.amt-rub)).
	oCRObCur:setH(op-entry.op-date,(ACCUM SUB-TOTAL BY op-entry.op-date op-entry.amt-cur)).
       END.	/* ����� ������ ��᫥���� ������� ��㯯� */
     END. /* ����� �� �஢����� */
   
/**************************************  ����� �������� �� �� *****************************/
IF val = 810 THEN DO:

 dItogRub  = dSum. 
 dBegPos   = dItogRub.
END.
ELSE DO:
  dItogCur  = dSum.
  dBegPos   = dItogCur.
END.


DO iDate = dLastPosDate TO dDate:

 oItogRub:setH(STRING(iDate),0).
 oItogRub:setH(STRING(iDate),DECIMAL(oItogRub:get(iDate)) + oSysClass:op-entry-sign("��",activity) * DECIMAL((IF oDBObRub:get(iDate) <> ? THEN oDBObRub:get(iDate) ELSE "0"))).
 oItogRub:setH(STRING(iDate),DECIMAL(oItogRub:get(iDate)) + oSysClass:op-entry-sign("��",activity) * DECIMAL((IF oCRObRub:get(iDate) <> ? THEN oCRObRub:get(iDate) ELSE "0"))).

 oItogCur:setH(STRING(iDate),0).
 oItogCur:setH(STRING(iDate),DECIMAL(oItogCur:get(iDate)) + oSysClass:op-entry-sign("��",activity) * DECIMAL((IF oDBObCur:get(iDate) <> ? THEN oDBObCur:get(iDate) ELSE "0"))).
 oItogCur:setH(STRING(iDate),DECIMAL(oItogCur:get(iDate)) + oSysClass:op-entry-sign("��",activity) * DECIMAL((IF oCRObCur:get(iDate) <> ? THEN oCRObCur:get(iDate) ELSE "0"))).

 dItogRub = dItogRub + DECIMAL(oItogRub:get(iDate)).
 dItogCur = dItogCur + DECIMAL(oItogCur:get(iDate)).

END. /* ����� �� �⮣���� ⠡��� */

DELETE OBJECT oDBObRub.
DELETE OBJECT oCRObRub.

DELETE OBJECT oDBObCur.
DELETE OBJECT oCRObCur.

DELETE OBJECT oSysClass.  

 IF val = 810 THEN DO:
   hCalc   = oItogRub.
   DELETE OBJECT oItogCur.
   RETURN dItogRub. 
 END.
 ELSE DO:
   hCalc   = oItogCur.
   DELETE OBJECT oItogRub.
   RETURN dItogCur.
 END.
END METHOD.

/********************
 * ��⮤ �����頥�
 * ���� ��᫥����� ��������
 * �� �㬬� , 祬 iLine
 ********************/
METHOD PUBLIC DATE getLastMoveByDate(INPUT begDate AS DATE,INPUT endDate AS DATE,INPUT iLine AS DECIMAL,INPUT cTransMask AS CHARACTER):

DEF VAR res AS DATE INITIAL 01/01/1900 NO-UNDO.


FOR EACH op-entry WHERE op-entry.acct-db EQ acct 
			 AND (op-entry.op-date >= begDate AND op-entry.op-date <= endDate AND op-entry.op-date <> ?)
			 AND amt-rub >= iLine NO-LOCK,
FIRST op OF op-entry WHERE CAN-DO(cTransMask,op.op-kind) NO-LOCK BY op.op-date:
	res = op-entry.op-date.
END.


FOR EACH op-entry WHERE op-entry.acct-cr EQ acct 
			 AND (op-entry.op-date >= begDate AND op-entry.op-date <= endDate AND op-entry.op-date <> ?)
			 AND amt-rub >= iLine NO-LOCK,
FIRST op OF op-entry WHERE CAN-DO(cTransMask,op.op-kind) NO-LOCK BY op.op-date:
	res = MAXIMUM(res,op-entry.op-date).
END.

  IF res EQ 01/01/1900 THEN res = ?.

  RETURN res.
END METHOD.

/**********************************************
  * �᫨ �� ���� �뫮 ��������, � ��⨭�.   *
  * ���� ����.                               *
  **********************************************/
METHOD PUBLIC LOGICAL hasMove(INPUT dBegDate AS DATE,INPUT dEndDate AS DATE,INPUT cTransMask AS CHARACTER):

  FOR EACH op-entry WHERE op-entry.acct-db EQ acct OR op-entry.acct-cr EQ acct 
	                  AND (op-entry.op-date >= dBegDate AND op-entry.op-date <= dEndDate AND op-entry.op-date <> ?) NO-LOCK,
          FIRST op OF op-entry WHERE CAN-DO(cTransMask,op.op-kind) NO-LOCK:
		RETURN TRUE.
        END.
  RETURN FALSE.
END METHOD.

DESTRUCTOR PUBLIC TAcct():
     /***********************************
      * � ⥡� ��ࠤ��, � ⥡� � ���!!!*
      ************************************/
END DESTRUCTOR.

END CLASS.