CLASS TClient INHERITS TBase:

DEF PROTECTED VAR iId  AS INTEGER   NO-UNDO.
DEF PROTECTED VAR cInn AS CHARACTER NO-UNDO.

{set-prop.i &aa="date-in"        &bb="dDate-In"        &cc="DATE"}
{set-prop.i &aa="cust-cat"       &bb="cType"           &cc="CHARACTER" &perms="PRIVATE"}
{set-prop.i &aa="name-short" &bb="cNameShort" &cc="CHARACTER" }
{set-prop.i &aa="address"      &bb="cAddress"      &cc="CHARACTER"}

DEF PUBLIC PROPERTY clInn AS CHARACTER
	GET:
		   /************************
		    * �������� ��ਠ��, ��
		    * � ������ ��� �� �㫨,
		    * �� ��⠥� ��થ஬,
		    * ������⢨� ���.
		    *************************/
	    IF NOT LOGICAL(INT64(cInn)) THEN RETURN ?.
					ELSE RETURN cInn.
	END.
	SET (INPUT cValue AS CHARACTER):
	  cInn = cValue.
	END.


CONSTRUCTOR TClient(INPUT type AS CHARACTER, INPUT id AS INT64):
            /*****************************
             * ��������� �����        *
             * �� ⨯� ������, � ��� ID *
             *****************************/
    cType = type.
    iId = id.
    Surrogate = STRING(id).
    initClass().
END CONSTRUCTOR.

CONSTRUCTOR TClient(INPUT cAcct AS CHARACTER):
           /********************************
            *                                   *
            * ��������� ����� �� ����  *
            *                                   *
            ********************************/
        DEF VAR oAcct AS TAcct     NO-UNDO.
        DEF VAR tmp   AS CHARACTER NO-UNDO.


        oAcct=new TAcct(cAcct).

         tmp = oAcct:clSurrogate.        
         cType=ENTRY(1,tmp,",") NO-ERROR.
         iId = INTEGER(ENTRY(2,tmp,",")) NO-ERROR.                

        

        DELETE OBJECT oAcct.
initClass().
END CONSTRUCTOR.

METHOD PRIVATE VOID initCh():
             /****************************
              * ���樠������� 䨧. ��� *
              ****************************/          
           FIND FIRST person WHERE person-id = iId NO-LOCK.           
           ASSIGN
                  clinn = person.inn
                  cNameShort = name-last + " " + first-names
                  dDate-In = person.date-in
		  address = person.address[1] + " " + person.address[2]
                  ClassCode = person.class-code
                .
END METHOD.
METHOD PRIVATE VOID initYu():
             /***************************
              * ���樠������� ��. ��� *
              ***************************/
            FIND FIRST cust-corp WHERE cust-id = iId NO-LOCK.

		/********************************************************
		 * ��� ��. ��� ����� ����� �㡫�஢�����.
		 * ���⮬� ��१��� �㡫�஢����.
		 ********************************************************/

            ASSIGN
                 clinn = cust-corp.inn
                 cNameShort = cust-corp.name-short
                 dDate-In = cust-corp.date-in		 
                 ClassCode = cust-corp.class-code
                 .        
/************************************************
 * ��⥫� ��� ����,                            *
 * �� ����稫��� ��� �ᥣ��.                    *
 * ������ addr-of-low[1] �� �������� �          *
 * ⮣�� REPLACE �뤠�� ᮮ�饭�� �� �訡��.... *
 * � ��㣮� ��஭� � �� �������, ��� �ࠢ���
 * ��������� ����� � ���⮬� ��� IF � ���
 * ⠪�� �������.
 ********************************************/
address = cust-corp.addr-of-low[1] + REPLACE(addr-of-low[2],addr-of-low[1],"") NO-ERROR.

END METHOD.

/*************************************
 * ���樠������� ᠬ�� ᥡ�.   *
 *************************************/
METHOD PRIVATE VOID initSelf():
	DEF VAR oSysClass AS TSysClass.

         oSysClass = new TSysClass().
		clinn = oSysClass:getSetting("���").
         DELETE OBJECT oSysClass.
END METHOD.
METHOD PRIVATE VOID initClass():
    Surrogate = STRING(iId).
    CASE cType:
       WHEN "�" THEN
                DO:
                   ClassType = "person".
                   initCh().  
                END.
       WHEN "�" THEN
                DO:
                  ClassType = "cust-corp".
                  initYu().
                END.

	/******** ���������� �� ���� **********/
	WHEN "�" THEN
		DO:
			ClassType="banks".
			initSelf().
		END.
     END CASE.  
END METHOD.
METHOD PRIVATE VOID getCustIdent(INPUT cCustCodeType AS CHARACTER):
/*   FIND FIRST cust-ident WHERE cust-ident=cCustCodeType.*/
END METHOD.

METHOD PUBLIC CHARACTER getInnByDate(INPUT dDate AS DATE):
	/**********************************************
	 *											   *
	 * �����頥� ��� � ��⮬ ⥬���஢������  *
         *											   *
	 **********************************************/

     DEF VAR tmpInn AS CHARACTER.

     tmpInn = getXAttr("inn",dDate).
     IF tmpInn EQ "" OR tmpInn EQ ? THEN RETURN clinn.
     ELSE RETURN tmpInn.
END METHOD.

END CLASS.
